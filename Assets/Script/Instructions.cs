﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instructions : MonoBehaviour {

	public GameObject a;
	public GameObject b;
	public GameObject c;
	public GameObject d;
	public GameObject e;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void a1()
	{
		a.SetActive(false);
		b.SetActive(true);
		c.SetActive(false);
		d.SetActive(false);
		e.SetActive(false);
	}
	public void b1()
	{
		a.SetActive(false);
		b.SetActive(false);
		c.SetActive(true);
		d.SetActive(false);
		e.SetActive(false);
	}
	public void c1()
	{
		a.SetActive(false);
		b.SetActive(false);
		c.SetActive(false);
		d.SetActive(true);
		e.SetActive(false);
	}
	public void d1()
	{
		a.SetActive(false);
		b.SetActive(false);
		c.SetActive(false);
		d.SetActive(false);
		e.SetActive(true);
	}

	public void e1()
	{
		Application.LoadLevel("Level1");

	}
	
}
